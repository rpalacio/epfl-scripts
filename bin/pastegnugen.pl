#!/usr/bin/env perl
# Copyright 2014 Florian Vessaz
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#     http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.
#
# Debian dependencies: libfile-mimeinfo-perl libio-stringy-perl libwww-perl
#
use strict;
use warnings;

use File::Basename;
use File::MimeInfo::Magic;
use IO::Scalar;
use LWP::UserAgent;
use Getopt::Long;

# TODO:
#  * Provide --help and -h option
#  * Don't store file in memory (and increase max_size)
#  * Show preview and ask for confirmation before paste unless --yes or -y is
#    given
#  * Allow to edit mimetype interactively when asking for confirmation
#  * Show human readable size

# Where to post the request:
my $url = 'https://paste.gnugen.ch/';

# Max allowed file size:
my $max_size = 1 * 1024 * 1024 * 1024; # 1Go
# Currently the file is stored in memory before being sent. This will be
# improved later.

# Mimetype, will be autodetected if left undefined
my $mimetype;

my $ua = LWP::UserAgent->new();

# If output is a terminal then be verbose (you may still use --quiet).
my $verbose = -t STDOUT;
my $expire;
GetOptions (
    "verbose"  => \$verbose,
    "quiet"    => sub { $verbose = 0 },
    "expire=s" => \$expire,
    "url=s"   => \$url,
    "mimetype=s" => \$mimetype,
);

$|++ if ($verbose); # Unbuffer STDOUT

if (@ARGV eq 0) {
    paste(\*STDIN);
} else {
    while (my $f = shift @ARGV) {
        if (open(my $fh, '<', $f)) {
            paste($fh, basename($f))
        } else {
            warn "Can't open $f: $!";
            next;
        }
    }
}

sub paste {
    my ($fh, $filename) = @_;
    $filename = '-' unless defined $filename;

    my $data;
    my $size = read($fh, $data, $max_size);

    unless (eof $fh) {
        warn "Refusing to paste $filename which is bigger than $max_size bytes";
        return;
    }

    my $io_scalar = IO::Scalar->new(\$data);
    $mimetype ||= mimetype($io_scalar);

    unless (defined $mimetype) {
        $mimetype = 'application/octet-stream';
        warn "Unable to guess mimetype of $filename, using $mimetype";
    }


    print "Pasting $filename ($mimetype) $size bytes ..." if $verbose;

    my $form = [ file => [undef, $filename, Content_Type => $mimetype,
                                            Content => $data]];
    push(@$form, expire => $expire) if $expire;
    my $response = $ua->post($url, Content_Type => 'form-data', Content => $form);

    if ($verbose) {
        print "\b"x4 if -t STDOUT;
        if ($response->is_success) {
            print ": "
        } else {
            print ". failed!\n";
        }
    }

    if ($response->is_success) {
        print $response->decoded_content;
    } else {
        die $response->status_line;
    }
}
