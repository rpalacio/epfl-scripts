#!/bin/sh
# Copyright 2014 Florian Vessaz
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#     http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.

help ()
{
    cat >&2 <<-EOF
    $0 [options] DEST

    Copy the executables found in the bin directory of this repository to DEST.
    Available options are:
      --symlink         Create symlinks instead of copying files.
      --force           Overwrite existing files.


    Exemples:

      Create symlinks in the 'bin' directory of your home:
        ./install.sh --symlink ~/bin

      Install to /usr/local/bin:
        ./install.sh /usr/local/bin
EOF
}

install_file ()
{
    if [ -e "$2" -a ! "$FORCE" = "YES" ]
    then
        printf "'$2' already exists.\n"
    else
        printf "Installing '$1' to '$2' ... "
        if [ "$FORCE" = "YES" ]
        then
            rm -f "$2"
        fi
        if [ "$SYMLINKS" = "YES" ]
        then
            ln -sf "$(readlink -e "$1")" "$2"
        else
            cp -f "$1" "$2"
        fi

        if [ "$?" = "0" ];
        then
            printf "\b\b\b\b\b. ok.\n"
        else
            printf "\nFailed to install '$1'!\n"
        fi
    fi
}

SYMLINKS="NO"
FORCE="NO"
while [ "$#" -gt "1" ]
do
    case "$1" in
        --symlink) SYMLINKS="YES" ;;
        --copy)    SYMLINKS="NO" ;;
        --force)   FORCE="YES" ;;
        *) help; exit 1 ;;
    esac
    shift
done
if [ "$#" = "1" ]
then
    DEST="$(readlink -m "$1")"
    if [ ! -d "$DEST" ]
    then
        printf "'$DEST' is not a directory."
        exit 1
    fi
else
    help
    exit 1
fi

# Change directory after parsing the argument this allow the user to pass a
# relative path.
cd "$(dirname "$(readlink -e "$0")")"

for f in bin/*
do
    if [ -f "$f" -a -e "$f" ]
    then
        install_file "$f" "$DEST/$(basename "$f")"
    fi
done

# vim: set et ts=4 sts=4 sw=4 :
