# EPFL Scripts

Scripts that are useful when you spend time at EPFL.


## Installation

After installing the dependencies (see below), the various script can either
be installed somewhere on your system or run directly from the source tree.

If you want to install them (by copying them or creating symlinks), you can
use the [`install.sh`](install.sh) script.

Arch Linux users can also simply install
[`epfl-scripts-git`](https://aur.archlinux.org/packages/epfl-scripts-git/) from
the AUR.


## Dependencies

For the shell scripts, the following software must be installed on your system:

  * coreutils
  * curl
  * file
  * openconnect
  * vpnc/vpnc-script
  * bash

If you want to use the network namespace jail feature, you will also need
  * iproute2
  * iptables

For the Perl scripts, the following modules must be installed on your system:

  * File::MimeInfo::Magic
  * HTML::TreeBuilder::XPath
  * HTML::TreeBuilder
  * IO::Scalar
  * LWP::UserAgent
  * Term::ANSIColor
  * WWW::Mechanize

For Debian (or Debian-based distributions like Ubuntu), the Perl modules are
provided by the following packages:

  * libfile-mimeinfo-perl
  * libhtml-treebuilder-xpath-perl
  * libhtml-tree-perl
  * libio-stringy-perl
  * libwww-perl
  * perl-modules
  * libwww-mechanize-perl


## Contributing

To ensure robustness, pushing directly to the `epfl-scripts` repository is not
permitted. When contributing, please fork `epfl-scripts` and make your changes
in your forked repository, then open a merge request, so that your changes can
undergo a brief review and feedback cycle before being applied.
